import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'UI/screens.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {


  
  @override
  Widget build(BuildContext context) {
    
    return MaterialApp(

      theme: ThemeData.light(),
      darkTheme: ThemeData.dark(),

      themeMode: ThemeMode.light,

      home:Scaffold(

        body: SafeArea(
          child: OrientationBuilder(

            builder: (context,orientation){
              if(orientation == Orientation.portrait)
              return StandardScreen();
              return ScientificScreen();
            },

          )
        ),

      ),
    );
  }
}
