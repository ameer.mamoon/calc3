import 'package:get/get.dart';

class CalcController extends GetxController{
  String _content = '';
  String _subcontent = '';

  CalcController._();

  static final CalcController controller = Get.put(CalcController._());

  String get content => _content;

  String get subcontent => _subcontent;



  void press(String value){
  _subcontent = _content;
  _content = value;
  update();
  }

}