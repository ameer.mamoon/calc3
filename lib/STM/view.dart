import 'package:calcolater/STM/controllers.dart';
import 'package:flutter/material.dart';
import 'dart:math';
import 'package:get/get.dart';


class OutputWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return LayoutBuilder(
        builder: (context,constrains) {
          double minSize = min(constrains.maxHeight,constrains.maxWidth);
          return Container(
            width: double.infinity,
            height: double.infinity,
            padding: EdgeInsets.all(minSize*0.025),
            child: GetBuilder<CalcController>(
              init: CalcController.controller,
              builder: (controller) {

                return Column(
                  children: [
                    _getElement(1, controller.subcontent),
                    SizedBox(
                      height: constrains.maxHeight*0.02,
                    ),
                    _getElement(2, controller.content),

                  ],
                );
              }
            ),
          );
        }
    );
  }

  Widget _getElement(int flex,String value){
    return Expanded(
        flex: flex,
        child: Container(
          width: double.infinity,
          height: double.infinity,
          child: FittedBox(child: Text(value),
            alignment: Alignment.centerLeft,
          ),
        )
    );
  }

}

