import 'package:calcolater/STM/controllers.dart';
import 'package:flutter/material.dart';
import 'dart:math';
import 'package:get/get.dart';

import '../main.dart';

class CalcButton extends StatelessWidget {
  final String value;


  CalcButton(this.value);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double minSize = min(size.height,size.width);

    return Expanded(
      child: GestureDetector(
        child: Container(
          decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.circular(minSize*0.025),
          ),
          width: double.infinity,
          height: double.infinity,
          margin: EdgeInsets.symmetric(vertical: size.height*0.01,
              horizontal: size.width*0.01
          ),
          child: Center(child: Text(value)),
        ),
        onTap: (){
          CalcController.controller.press(value);
          print(value);

        },
      ),
    );
  }
}

