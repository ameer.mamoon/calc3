import 'package:calcolater/STM/controllers.dart';
import 'package:calcolater/STM/view.dart';
import 'package:calcolater/UI/elements.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:calcolater/STM/controllers.dart';

class StandardScreen extends StatelessWidget {



  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Column(
      children: [
        Expanded(
          flex: 2,
            child: OutputWidget(),
        ),
        Expanded(
            child: Row(
              children: [
                CalcButton('='),
                CalcButton('DEL'),
              ],
            )
        ),
        Expanded(
            child: Row(
              children: [
                CalcButton('7'),
                CalcButton('8'),
                CalcButton('9'),
                CalcButton('÷'),
              ],
            )
        ),
        Expanded(
            child: Row(
              children: [
                CalcButton('4'),
                CalcButton('5'),
                CalcButton('6'),
                CalcButton('×'),
              ],
            )
        ),
        Expanded(
            child: Row(
              children: [
                CalcButton('1'),
                CalcButton('2'),
                CalcButton('3'),
                CalcButton('-'),
              ],
            )
        ),
        Expanded(
            child: Row(
              children: [
                CalcButton('.'),
                CalcButton('0'),
                CalcButton('C'),
                CalcButton('+'),
              ],
            )
        ),
      ],
    );
  }
}



class ScientificScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text('Scientific Screen'),
    );
  }
}